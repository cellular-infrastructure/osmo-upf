/* GTP-U ECHO implementation for osmo-upf */
#pragma once

int upf_gtpu_echo_setup(struct upf_gtp_dev *dev);
int upf_gtpu_echo_req_tx(struct upf_gtp_dev *dev, const struct osmo_sockaddr *remote, uint16_t seq_nr);
