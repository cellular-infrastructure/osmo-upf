/*
 * (C) 2021-2022 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
 * All Rights Reserved.
 *
 * Author: Neels Janosch Hofmeyr <nhofmeyr@sysmocom.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdint.h>

#include <osmocom/core/hashtable.h>
#include <osmocom/core/use_count.h>

#include <osmocom/pfcp/pfcp_msg.h>

struct osmo_fsm_inst;
struct osmo_pfcp_msg;
struct up_peer;

enum up_session_fsm_event {
	UP_SESSION_EV_RX_SESSION_EST_REQ,
	UP_SESSION_EV_RX_SESSION_MOD_REQ,
	UP_SESSION_EV_RX_SESSION_DEL_REQ,
	UP_SESSION_EV_USE_COUNT_ZERO,
};

struct up_session {
	struct hlist_node node_by_up_seid;
	struct hlist_node node_by_cp_seid;

	/* item in up_endpoint->peers_by_up_seid: */
	struct hlist_node ep_node_by_up_seid;

	struct osmo_fsm_inst *fi;
	/* backpointer */
	struct up_peer *up_peer;

	struct osmo_pfcp_ie_f_seid cp_f_seid;
	uint64_t up_seid;

	struct osmo_use_count use_count;
	struct osmo_use_count_entry use_count_buf[8];

	/* llist of struct pdr */
	struct llist_head pdrs;
	/* llist of struct far */
	struct llist_head fars;
	/* llist of struct chosen_f_teid */
	struct llist_head chosen_f_teids;

	/* llist of struct up_gtp_action */
	struct llist_head active_gtp_actions;
};

struct up_session *up_session_find_or_add(struct up_peer *peer, const struct osmo_pfcp_ie_f_seid *cp_f_seid);
struct up_session *up_session_find_by_up_seid(struct up_peer *peer, uint64_t up_seid);
struct up_session *up_session_find_by_cp_f_seid(struct up_peer *peer, const struct osmo_pfcp_ie_f_seid *cp_f_seid);

void up_session_set_msg_ctx(struct up_session *session, struct osmo_pfcp_msg *m);

char *up_session_gtp_status(struct up_session *session);
bool up_session_is_active(struct up_session *session);
bool up_session_is_fully_active(struct up_session *session, int *active_p, int *inactive_p);

int up_session_discard(struct up_session *session);

int up_session_to_str_buf(char *buf, size_t buflen, struct up_session *session);
char *up_session_to_str_c(void *ctx, struct up_session *session);

struct pdr {
	struct llist_head entry; /* item in session->pdrs */
	struct hlist_node node_by_local_f_teid; /* item in g_upf->gtp.pdrs_by_local_f_teid */
	struct up_session *session;

	struct osmo_pfcp_ie_create_pdr desc;
	struct osmo_pfcp_ie_f_teid *local_f_teid;
	struct osmo_pfcp_ie_f_teid _local_f_teid_buf;

	struct far *far;

	bool rx_decaps;
	bool forw_encaps;
	bool access_to_core;
	bool core_to_access;

	struct pdr *reverse_pdr;
	bool active;

	char *inactive_reason;
};

int pdr_to_str_buf(char *buf, size_t buflen, const struct pdr *pdr);
char *pdr_to_str_c(void *ctx, const struct pdr *pdr);

struct far {
	struct llist_head entry;
	struct up_session *session;

	struct osmo_pfcp_ie_create_far desc;
	bool active;
};
