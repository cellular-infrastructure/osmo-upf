/*
 * (C) 2021-2022 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
 * All Rights Reserved.
 *
 * Author: Neels Janosch Hofmeyr <nhofmeyr@sysmocom.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <osmocom/core/linuxlist.h>
#include <osmocom/core/hashtable.h>

struct osmo_pfcp_msg;
struct osmo_pfcp_endpoint;
struct osmo_sockaddr;

#define UP_USE_MSG_RX "msg-rx"
#define UP_USE_MSG_TX "msg-tx"

struct up_endpoint {
	struct osmo_pfcp_endpoint *pfcp_ep;

	/* list of struct up_peer. */
	struct llist_head peers;
	/* hashtable of (struct up_session) with key up_seid.
	 * Allows quick access to sessions (and its endpoint as backpointer)
	 * with a given up_seid. */
	DECLARE_HASHTABLE(sessions_by_up_seid, 10);

	uint64_t next_up_seid_state;
};

struct up_endpoint *up_endpoint_alloc(void *ctx, const struct osmo_sockaddr *local_addr);
int up_endpoint_bind(struct up_endpoint *up_ep);
void up_endpoint_free(struct up_endpoint **ep);

uint64_t up_endpoint_next_up_seid(struct up_endpoint *ep);
