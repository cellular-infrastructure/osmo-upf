/*
 * (C) 2022 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
 * All Rights Reserved.
 *
 * Author: Neels Janosch Hofmeyr <nhofmeyr@sysmocom.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <osmocom/core/linuxlist.h>
#include <osmocom/core/sockaddr_str.h>

struct vty;

struct network_instance {
	struct llist_head entry;

	char *name;
	struct osmo_sockaddr_str addr;
};

const struct network_instance *netinst_add(void *ctx, struct llist_head *list, const char *name, const char *addr,
					   const char **errmsg);
const struct network_instance *netinst_find(struct llist_head *list, const char *name);
const struct network_instance *netinst_first(struct llist_head *list);
int netinst_clear(struct llist_head *list);

int netinst_vty_write(struct vty *vty, struct llist_head *list, const char *indent, const char *name_or_null);
