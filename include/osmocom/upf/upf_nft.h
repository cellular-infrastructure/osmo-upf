/*
 * (C) 2021-2022 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
 * All Rights Reserved.
 *
 * Author: Neels Janosch Hofmeyr <nhofmeyr@sysmocom.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdint.h>
#include <osmocom/core/hashtable.h>

#include <osmocom/upf/upf_tun.h>

struct upf_nft_tun {
	struct hlist_node node_by_chain_id; /* item in g_upf->tunmap.nft_tun_by_chain_id */
	struct upf_tun tun;
	uint32_t chain_id;
};

struct upf_tunmap {
	struct upf_nft_tun access;
	struct upf_nft_tun core;
};

int upf_nft_tunmap_to_str_buf(char *buf, size_t buflen, const struct upf_tunmap *tunmap);
char *upf_nft_tunmap_to_str_c(void *ctx, const struct upf_tunmap *tunmap);

int upf_nft_init();
int upf_nft_free();

char *upf_nft_tunmap_get_table_init_str(void *ctx);
char *upf_nft_tunmap_get_vmap_init_str(void *ctx);
char *upf_nft_tunmap_get_ruleset_str(void *ctx, struct upf_tunmap *tunmap);
char *upf_nft_tunmap_get_ruleset_del_str(void *ctx, struct upf_tunmap *tunmap);
int upf_nft_tunmap_create(struct upf_tunmap *tunmap);
int upf_nft_tunmap_delete(struct upf_tunmap *tunmap);
