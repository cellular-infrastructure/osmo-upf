/*
 * (C) 2021-2022 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
 * All Rights Reserved.
 *
 * Author: Neels Janosch Hofmeyr <nhofmeyr@sysmocom.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdint.h>

#include <osmocom/core/linuxlist.h>
#include <osmocom/core/socket.h>
#include <osmocom/core/fsm.h>

#include <osmocom/upf/up_session.h>
#include <osmocom/upf/upf_gtp.h>
#include <osmocom/upf/upf_nft.h>

#define LOG_UP_GTP_ACTION(A, LEVEL, FMT, ARGS...) \
	LOGP(DGTP, LEVEL, "%s: " FMT, up_gtp_action_to_str_c(OTC_SELECT, A), ##ARGS)

struct up_session;

enum up_gtp_action_kind {
	UP_GTP_DROP,
	UP_GTP_U_TUNEND,
	UP_GTP_U_TUNMAP,
};

struct up_gtp_action {
	struct llist_head entry;
	struct up_session *session;

	uint16_t pdr_access;
	uint16_t pdr_core;

	enum up_gtp_action_kind kind;
	union {
		/* En-/De-capsulate GTP: add/remove a GTP header and forward the GTP payload from/to plain IP. */
		struct upf_tunend tunend;

		/* Tunnel-map GTP: translate from one TEID to another and forward */
		struct upf_tunmap tunmap;
	};

	/* volatile loop variable to match up wanted and actually present GTP actions */
	void *handle;
};

struct up_gtp_action *up_gtp_action_alloc(void *ctx, struct up_session *session, enum up_gtp_action_kind kind, struct llist_head *dst);
void up_gtp_action_free(struct up_gtp_action *a);

int up_gtp_action_cmp(const struct up_gtp_action *a, const struct up_gtp_action *b);

int up_gtp_action_enable(struct up_gtp_action *a);
int up_gtp_action_disable(struct up_gtp_action *a);

int up_gtp_action_to_str_buf(char *buf, size_t buflen, const struct up_gtp_action *a);
char *up_gtp_action_to_str_c(void *ctx, const struct up_gtp_action *a);
