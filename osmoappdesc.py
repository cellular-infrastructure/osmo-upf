#!/usr/bin/env python3

# (C) 2021-2022 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

app_configs = {
    "osmo-upf": ["doc/examples/osmo-upf/osmo-upf-mockup.cfg"]
}

apps = [(4275, "src/osmo-upf/osmo-upf", "OsmoUPF", "osmo-upf")
        ]

vty_command = ["./src/osmo-upf/osmo-upf", "-c",
               "doc/examples/osmo-upf/osmo-upf-mockup.cfg"]

vty_app = apps[0]
