:gfdl-enabled:
:program-name: OsmoUPF

OsmoUPF User Manual
===================
Neels Janosch Hofmeyr <nhofmeyr@sysmocom.de>

include::./common/chapters/preface.adoc[]

include::{srcdir}/chapters/overview.adoc[]

include::{srcdir}/chapters/running.adoc[]

include::{srcdir}/chapters/netinst.adoc[]

include::./common/chapters/vty.adoc[]

include::./common/chapters/logging.adoc[]

include::./common/chapters/counters-overview.adoc[]

include::./common/chapters/control_if.adoc[]

include::./common/chapters/vty_cpu_sched.adoc[]

include::./common/chapters/port_numbers.adoc[]

include::./common/chapters/bibliography.adoc[]

include::./common/chapters/glossary.adoc[]

include::./common/chapters/gfdl.adoc[]
