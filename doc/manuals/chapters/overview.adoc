OsmoUPF

[[overview]]
== Overview

This manual should help you getting started with OsmoUPF. It will cover
aspects of configuring and running OsmoUPF.

[[intro_overview]]
=== About OsmoUPF

OsmoUPF is the Osmocom implementation of a User Plane Function for 2G, 3G, 4G
and 5G mobile network packet switched user data management. It implements:

- A Packet Forwarding Control Protocol (PFCP) entity to manage the GTP user
  plane of mobile subscribers.
- GTP forwarding as well as encapsulation/decapsulation of user traffic, using
  the Linux mainline kernel GTP module.

The aim is to provide:

- 1000 modifications of tunnel state per second (add/remove/modify),
- 4-8 Gbps throughput,
- 100-125k concurrent GTP tunnels.

A typical network scenario using OsmoUPF is illustrated in the following
diagram:

.Typical network architecture used with OsmoUPF
[graphviz]
----
digraph G {
  rankdir = LR;

  UE [label="UE\n(3G phone)"]

  subgraph cluster_hnbgw_mgw_upf {
    style=dotted
    HNBGW -> UPF [label="PFCP",constraint=false]
    UPF [label=OsmoUPF,style=bold]
  }

  subgraph cluster_hnbgw_mgw_upf2 {
    style=dotted
    SGSN -> UPF2 [label="PFCP",constraint=false]
    UPF2 [label=OsmoUPF,style=bold]
  }

  subgraph cluster_hnbgw_mgw_upf3 {
    style=dotted
    GGSN -> UPF3 [label="PFCP",constraint=false]
    UPF3 [label=OsmoUPF,style=bold]
  }

  hNodeB [shape="box",label="hNodeB\n(3G femto cell)"]

  UE -> hNodeB [label="Uu"]
  hNodeB -> HNBGW [label="Iuh",style=dashed]
  STP [label="STP\n(SCCP/M3UA)"]
  HNBGW -> STP -> SGSN [label="IuPS",style=dashed]
  SGSN -> GGSN [label="GTP-C",style="dashed"]
  hNodeB -> UPF -> UPF2 -> UPF3 [label="GTP-U"]
  UPF3 -> internet [label="apn"]
}
----

NOTE: at the time of writing this section, the only Osmocom component providing
a PFCP CPF interface is OsmoHNBGW. PFCP support has not yet made its way into
OsmoSGSN nor OsmoGGSN.

=== the PFCP interface

PFCP is specified by 3GPP TS 29.244.

OsmoUPF implements a PFCP User Plane Function interface, listening for PFCP
requests from PFCP Control Plane Function clients, to carry out proxy-relaying
and encapsulation/decapsulation of GTP tunnels.

OsmoUPF does not support the complete PFCP feature set. It detects exactly two
use cases that will provide service of actual GTP tunnels:

.tunend use case
----
Access                 osmo-upf              Core
 PGW                      |              PDN/internet
  |                PDR1:  > FAR1:             |
  |                IP/GTP | IP                |
  |        ------> F-TEID |            -----> |
  |                       |                   |
  |                FAR2:  < PDR2:             |
  |                IP/GTP | IP                |
  | F-TEID <------        | UE IP addr <----- |
----

* `tunend`: GTP tunnel encapsulation/decapsulation:
  - One Packet Detection Rule (PDR) accepts a GTP tunnel from the Access side
    with an Outer Header Removal.
  - This PDR uses a Forwarding Action Rule (FAR) for plain IP towards Core.
  - Another PDR accepts plain IP on a specific IP address from Core.
  - The second PDR uses a FAR towards Access with Outer Header Creation for GTP.

.tunmap use case
----
Access                 osmo-upf                 Core
 PGW                      |                     PGW
  |                PDR1:  > FAR1:                |
  |                IP/GTP | IP/GTP               |
  |        ------> F-TEID |        -----> F-TEID |
  |                       |                      |
  |                FAR2:  < PDR2:                |
  |                IP/GTP | IP/GTP               |
  | F-TEID <------        | F-TEID <-----        |
----

* `tunmap`: GTP tunnel forwarding:
  - One Packet Detection Rule (PDR) accepts a GTP tunnel from the Access side
    with an Outer Header Removal.
  - This PDR uses a Forwarding Action Rule (FAR) towards Core with an Outer
    Header Creation for GTP.
  - A second PDR+FAR pair like above, with Access and Core swapped.

Access and Core must be indicated by the Source Interface IE (PDR) and
Destination Interface IE (FAR) in PFCP.

Any set of rules only partially or not at all matching the above PDR and FAR
rules will not result in any actions on the GTP user plane, but will still
return a successful outcome in the PFCP messages.

For example, a rule set using a Source Interface other than "Access" or "Core" results
in a PFCP no-op, returning PFCP responses with successful outcome, but not
providing any GTP-U service.

This is a direct result of:

- allowing PFCP rule sets to be setup incrementally by several subsequent PFCP
  messages, and of
- OsmoUPF using Linux kernel features for the GTP user plane, where there is
  either a full bidirectional GTP tunnel in place or none at all.

For example, for `tunmap`, a typical CPF will establish a PFCP session in two
steps: first request a local F-TEID from the UPF before passing on a data
service request from Access to Core. When the Core side has responded with its
GTP details, the PFCP session at the UPF is updated (Session Modifification),
to form a complete PFCP rule set.

.Typical sequence of establishing a GTP-U tunnel relay
["mscgen"]
----
msc {
	hscale="1";
	sgsn[label="SGSN"],sgwc[label="SGW-C"],sgwu[label="SGW-U"],pgwc[label="PGW-C"];

	sgsn << pgwc [label="Access"];
	sgsn >> pgwc [label="Core"];

	sgsn => sgwc [label="GTP Create Session Request\n\n\n"];

	|||;

	sgwc => sgwu [label="PFCP Session Establishment Request\n\n2x Create PDR\nF-TEID = CHOOSE"];

	|||;

	sgwc <= sgwu [label="PFCP Session Establishment Response\n\n2x Created PDR\nwith chosen local F-TEID"];

	|||;

	sgwc => pgwc [label="GTP Create Session Request\nwith chosen local F-TEID towards Core"];
	sgwc <= pgwc [label="GTP Create Session Response\nwith remote F-TEID at Core"];

	|||;

	sgwc => sgwu [label="PFCP Session Modification Request\n\nUpdate FAR\nwith remote F-TEID at Core"];

	|||;

	sgwc <= sgwu [label="PFCP Session Modification Response\n\n\n"];

	|||;

	sgsn <= sgwc [label="GTP Create Session Response\n\n\n"];
}
----

The OsmoUPF logging as well as the VTY interface yield information on whether a
ruleset results in an actual bidirectional GTP tunnel being set up.


=== the GTP interface

OsmoUPF requires the following Linux kernel features to provide the GTP user
plane functionality:

- the Linux kernel GTP module for encapsulation/decapsulation between GTP and
  plain IP.
- the Linux netfilter nftables feature for relaying GTP, i.e. forwarding between
  two GTP tunnels.

Tunnel relaying with netfilter requires at least Linux kernel 5.17.

To be able to interact with these Linux kernel features, the osmo-upf binary
needs cap_net_admin privileges, as in:

----
sudo setcap cap_net_admin+pe /usr/bin/osmo-upf
----

Without above Linux kernel features, or when no cap_net_admin is available,
OsmoUPF is only useful for testing PFCP clients: the GTP features may be run in
mockup mode, so that OsmoUPF serves as a "dry run" PFCP server.
