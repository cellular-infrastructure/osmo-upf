#!/bin/sh
set -x
bin="${1:-$(which osmo-upf)}"
setcap cap_net_admin+pe "$bin"
