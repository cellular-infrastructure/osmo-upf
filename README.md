osmo-upf - Osmocom User Plane Function Implementation
=====================================================

Homepage
--------

The official homepage of the project is
https://osmocom.org/projects/osmo-upf/wiki

GIT Repository
--------------

You can clone from the official osmo-upf.git repository using

	git clone https://gitea.osmocom.org/cellular-infrastructure/osmo-upf

There is a web interface at https://gitea.osmocom.org/cellular-infrastructure/osmo-upf.

To submit patches, see "Contributing" below.

Documentation
-------------

User Manuals and VTY reference manuals are [optionally] built in PDF form
as part of the build process.

Pre-rendered PDF version of the current "master" can be found at
[User Manual](https://ftp.osmocom.org/docs/latest/osmoupf-usermanual.pdf)
as well as the [VTY Reference Manual](https://ftp.osmocom.org/docs/latest/osmoupf-vty-reference.pdf)


Mailing List
------------

Discussions related to osmo-bts are happening on the
osmocom-net-gprs@lists.osmocom.org mailing list, please see
https://lists.osmocom.org/postorius/lists/osmocom-net-gprs@lists.osmocom.org/
for subscription options and the list archive.

Please observe the [Osmocom Mailing List
Rules](https://osmocom.org/projects/cellular-infrastructure/wiki/Mailing_List_Rules)
when posting.

Contributing
------------

Our coding standards are described at
https://osmocom.org/projects/cellular-infrastructure/wiki/Coding_standards

Submit patches at https://gerrit.osmocom.org/
See also https://osmocom.org/projects/cellular-infrastructure/wiki/Gerrit

The current patch queue for OsmoUPF can be seen at
https://gerrit.osmocom.org/#/q/project:osmo-upf+status:open
